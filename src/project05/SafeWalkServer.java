package project05;

/**
 * @author Alexander Coll, acoll, 804
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Arrays;

public class SafeWalkServer implements Runnable {

    private int port;
    private ArrayList<Client> clientList = new ArrayList<Client>();
    private ServerSocket ss;

    private String[] locations = { "CL50", "EE", "LWSN", "PMU", "PUSH", "*" };

    /**
     * Construct the server, set up the socket.
     * 
     * @throws SocketException
     *             if the socket or port cannot be obtained properly.
     * @throws IOException
     *             if the port cannot be reused.
     */
    public SafeWalkServer(int port) throws SocketException, IOException {
        if (port >= 1025 && port <= 65535) {
            this.port = port;
            ServerSocket ss = new ServerSocket(this.port);
            this.ss = ss;
        } else {
            System.out.println("Error: Port must be an integer between 1025 and 65535 (inclusive)");
            return;
        }

    }

    /**
     * Construct the server and let the system allocate it a port.
     * 
     * @throws SocketException
     *             if the socket or port cannot be obtained properly.
     * @throws IOException
     *             if the port cannot be reused.
     */
    public SafeWalkServer() throws SocketException, IOException {
        ServerSocket ss = new ServerSocket(0);
        this.port = ss.getLocalPort();
        System.out.println(this.port);
        this.ss = ss;
    }

    /**
     * Return the port number on which the server is listening.
     */
    public int getLocalPort() {
        return this.port;
    }

    /**
     * 2 classess in same file, because coding standards don't exist.
     */
    @SuppressWarnings("unused")
    private class Client {
        public Socket socket;
        private String name;
        private String from;
        private String to;
        private int type;
        private String[] inputFields;

        public Client(Socket socket, String[] inputFields) {
            this.inputFields = inputFields;
            this.socket = socket;
            this.name = inputFields[0];
            this.from = inputFields[1];
            this.to = inputFields[2];
            this.type = Integer.parseInt(inputFields[3]);
        }

        public String getName() {
            return this.name;
        }

        public String getFrom() {
            return this.from;
        }

        public String getTo() {
            return this.to;
        }

        public int getType() {
            return this.type;
        }
    }

    public String[][] getPendingRequests() {
        // TODO listPendingRequest()
        String[][] pendingRequests = new String[clientList.size()][4];
        for (int i = 0; i < clientList.size(); i++) {
            pendingRequests[i] = clientList.get(i).inputFields;
        }
        return pendingRequests;
    }

    public boolean isLocationValid(String location) {
        for (int i = 0; i < locations.length; i++) {
            if (location.equals(locations[i])) {
                return true;
            }
        }
        return false;
    }

    public boolean isRequestValid(String[] request) {
        // Check if From location is valid
        if (!isLocationValid(request[1]))
            return false;
        // Check if To location is valid
        if (!isLocationValid(request[2]))
            return false;
        if (request[1].equals("*"))
            return false;
        if (request[1].equals(request[2]))
            return false;

        // Else
        return true;
    }

    public Client searchForMatch(Client requester) {
        for (int i = 0; i < clientList.size(); i++) {
            Client potentialClient = clientList.get(i);
            if (!(potentialClient.to.equals("*") && requester.to.equals("*"))) {
                if (potentialClient.from.equals(requester.from) && potentialClient.to.equals(requester.to)) {
                    System.out.println("searchForMatch(): match found");
                    return potentialClient;
                }
                if (potentialClient.from.equals(requester.from) && potentialClient.to.equals("*")) {
                    System.out.println("searchForMatch(): match found");
                    return potentialClient;
                }
                if (potentialClient.from.equals(requester.from) && requester.to.equals("*")) {
                    System.out.println("searchForMatch(): match found");
                    return potentialClient;
                }
            }
        }
        return null;
    }

    public int getClientIndex(Client client) {
        for (int i = 0; i < this.clientList.size(); i++) {
            if (client == this.clientList.get(i)) {
                return i;
            }
        }
        return -1;
    }

    public void commandReset() throws IOException {
        for (int i = 0; i < clientList.size(); i++) {
            Client client = clientList.get(i);
            PrintWriter out = new PrintWriter(client.socket.getOutputStream(), true);
            System.out.println("ERROR: connection reset");
            out.println("ERROR: connection reset");
            out.close();
            client.socket.close();
            clientList.remove(i);
        }
        clientList.trimToSize();

    }

    /**
     * Start a loop to accept incoming connections.
     */
    public void run() {
        try {
            while (!ss.isClosed()) {
                Socket client = ss.accept();
                PrintWriter out = new PrintWriter(client.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                String message = in.readLine();
                if (message.charAt(0) == ':') {
                    // If it is a command
                    System.out.println("is command");
                    String command = message.substring(1);
                    if (command.equals("LIST_PENDING_REQUESTS")) {
                        // List pending requests
                        System.out.println("Command: LIST_PENDING_REQUESTS");
                        out.println(Arrays.deepToString(getPendingRequests()));
                    } else if (command.equals("RESET")) {
                        // Reset server
                        System.out.println("Command: RESET");
                        out.println("RESPONSE: success");
                        commandReset();
                    } else if (command.equals("SHUTDOWN")) {
                        // Shutdown server
                        System.out.println("Command: SHUTDOWN");
                        out.println("RESPONSE: success");
                        commandReset();
                        ss.setReuseAddress(true);
                        ss.close();
                    } else {
                        System.out.println("Command not valid: " + command);
                        out.println("ERROR: invalid request");
                    }
                    in.close();
                    out.close();
                    client.close();
                } else {
                    // Try if it is a request
                    String[] inputFields = message.split(",");
                    if (inputFields.length == 4) {
                        System.out.println("is request");
                        if (isRequestValid(inputFields)) {
                            System.out.println("Request is valid");

                            Client currentClient = new Client(client, inputFields);
                            Client client2 = searchForMatch(currentClient);
                            if (client2 == null) {
                                System.out.println("No match found");
                                clientList.add(new Client(client, inputFields));
                            } else {
                                System.out.println("Match found");
                                PrintWriter out2 = new PrintWriter(client2.socket.getOutputStream(), true);
                                String response1 = "RESPONSE: " + client2.name + "," + client2.from + "," + client2.to
                                        + "," + client2.type;
                                System.out.println(response1);

                                String response2 = "RESPONSE: " + currentClient.name + "," + currentClient.from + ","
                                        + currentClient.to + "," + currentClient.type;

                                System.out.println(response2);
                                out.println(response1);
                                out2.println(response2);
                                out.close();
                                out2.close();
                                in.close();
                                client.close();
                                client2.socket.close();
                                clientList.remove(getClientIndex(client2));
                                clientList.trimToSize();
                            }
                        } else {
                            System.out.println("ERROR: invalid request");
                            out.println("ERROR: invalid request");
                            out.close();
                            client.close();
                        }
                    } else {
                        System.out.println("ERROR: invalid request");
                        out.println("ERROR: invalid request");
                        out.close();
                        client.close();
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Check if a String is an integer
     * 
     * @param s
     *            String to be checked
     * @return true if integer, false if not an integer
     */
    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
        if (args.length == 0) {
            System.out.println("Port not specified. Using free port");
            SafeWalkServer server = new SafeWalkServer();
            server.run();
        } else if (args.length >= 1) {
            if (args.length > 1) {
                System.out.println("Info: Only 1 argument is accepted. Ignoring arguments after port");
            }
            if (isInteger(args[1])) {
                int port = Integer.parseInt(args[1]);
                if (port >= 1025 && port <= 65535) {
                    SafeWalkServer server = new SafeWalkServer(Integer.parseInt(args[1]));
                    server.run();
                } else {
                    System.out.println("Error: Port must be an integer between 1025 and 65535 (inclusive)");
                    return;
                }
            } else {
                System.out.println("Error: Port must be an integer between 1025 and 65535 (inclusive)");
                return;
            }
        }
    }
}
