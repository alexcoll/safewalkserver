# cs180-project05 #
## Safe Walk ##
Safe Walk is a campus safety system (currently done manually) that allows people on campus to request that someone walk with them from one location to another on campus.

## Project ##
In the projects that follow, you will implement and expand a server and an Android app for the Purdue Safe Walk Program. The server you are going to write will handle requests sent from the user, and the app will provide the user with an easy way to organize and send requests and to receive response messages.

## Defintions ##
### Locations ###
* CL50: Class of 1950 Lecture Hall
* EE: Electrical Engineering Building
* LWSN: Lawson Computer Science Building
* PMU: Purdue Memorial Union
* PUSH: Purdue University Student Health Center
* "*": Any one of the location above
